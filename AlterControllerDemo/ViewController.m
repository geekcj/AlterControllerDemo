//
//  ViewController.m
//  AlterControllerDemo
//
//  Created by zjsruxxxy3 on 15/4/28.
//  Copyright (c) 2015年 WR. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSString *_psd;
    
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self addAlterControllerWithActionSheet];
}

-(void)addAlterControllerWithAlter
{
    // 创建一个alterController;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"WR'sAlter" message:@"This is an alter" preferredStyle:UIAlertControllerStyleAlert];
    
//    UIAlertActionStyleDefault
    UIAlertAction *destructive = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {

    }];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Default" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([_psd isEqualToString:@"wrcj12138"])
        {
            NSLog(@"correct !!!");
            
        }else
        {
            NSLog(@"wrong !!!");
            _psd = nil;
            
        }
    }];
    
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(test:) name:UITextFieldTextDidChangeNotification object:nil];
        
        
    }];
  
    

    [alert addAction:defaultAction];

    [alert addAction:destructive];
    
    [self presentViewController:alert animated:YES completion:^{
        
        
    }];
    
}

-(void)test:(NSNotification *)notification
{
    UITextField *textField = (UITextField *)notification.object;
    
    NSLog(@"%@",textField.text);
    
    _psd = textField.text;
    
 
}

-(void)addAlterControllerWithActionSheet
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"WR'sAlter" message:@"This is an alter" preferredStyle:UIAlertControllerStyleActionSheet];
    
    //    UIAlertActionStyleDefault
    UIAlertAction *destructive = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        NSLog(@"destructive");

    }];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Default" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"Default");

    }];
    
    [alert addAction:defaultAction];
    [alert addAction:destructive];
    
    [self presentViewController:alert animated:YES completion:^{
        
        NSLog(@"actionSheet");

    }];
    

}


@end
