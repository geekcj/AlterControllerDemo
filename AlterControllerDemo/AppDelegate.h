//
//  AppDelegate.h
//  AlterControllerDemo
//
//  Created by zjsruxxxy3 on 15/4/28.
//  Copyright (c) 2015年 WR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

